module.exports = {
    "env": {
        "browser": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parser": "babel-eslint",
    // "parserOptions": {
    //     "ecmaVersion": 5
    // },
    "rules": {
        "indent": [
            "error",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-unused-vars": [
            "warn",
            {
                "ignoreRestSiblings": true,
                "destructuredArrayIgnorePattern": "[A-Z]",
                "caughtErrors": "none"
            }
        ]
    }
};