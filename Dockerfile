FROM node:20 as builder
WORKDIR /app
COPY . .

RUN npm run build:ci

FROM nginx:stable-alpine-slim as production
ENV NODE_ENV production

COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]